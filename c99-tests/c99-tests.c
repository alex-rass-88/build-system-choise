#include <stdbool.h>
#include <stdio.h>

// object for test
struct obj {
  float pos[2];
  float color[4];
  const char *name;
};

// constructor
#define obj_construct(_name, ...) \
  (struct obj){                   \
      .name = _name, .pos = {0, 0}, .color = {1, 1, 1, 1}, __VA_ARGS__};

// pretty printer
void print_obj(struct obj *o) {
  printf("Object: [%s]\n", o->name);
  printf("\tPosition: [%f, %f]\n", o->pos[0], o->pos[1]);
  printf("\tColor: [%f, %f, %f]\n", o->color[0], o->color[1], o->color[2]);
}

int main() {
  struct obj o1 = obj_construct("object_1", .pos = {0, 10});
  struct obj o2 = obj_construct("object_2", .color = {1, 0, 0, 1});
  struct obj o3 = obj_construct("object_3");
  struct obj o4 =
      obj_construct("object_4", .pos = {2, 7}, .color = {1, 0.5f, 0.4f, 0});

  print_obj(&o1);
  print_obj(&o2);
  print_obj(&o3);
  print_obj(&o4);

  return 0;
}
