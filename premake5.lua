projectPath = "_project_" .. os.target()

workspace "funtests"
language "C"
configurations {"Debug", "Release"}
platforms {"x64", "x32"}

location(projectPath)
targetdir(path.join(projectPath, "bin"))
debugdir(path.join(projectPath, "bin"))
objdir(path.join(projectPath, "obj"))

include("c99-tests/premake5.lua")
